<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'wordpress');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@hO-s;J%*{0+AH:8=~(y0[@n+BfDZ$W:KSR-x:/eh5np*q}YW`[<T!<l|r(6|=W6');
define('SECURE_AUTH_KEY',  'sXm$D:5dQW:m|g7hT1qoh}#Oas0NtwTu<&!RY(c+Z7?DXm,4.NGi:!OMH,Q%5XbH');
define('LOGGED_IN_KEY',    ']rkX.<?%KXfJa!,4FcoN{z2#6i)= <{SBi_On-*?`t#CrnETHNx> Oh3C7,um7gW');
define('NONCE_KEY',        'DRdo4*[q2{V$w%qvmj/#R5`w[P$phfU97s`&TEnbVCD:Y[2EYJlW2-2bUvM&];9f');
define('AUTH_SALT',        '^,RBX)GDi-O^2+(b2=cp?/O|MPBV+]_Cp48%mMkd9[5x,iVb/,wO)@|nL/k+DXf|');
define('SECURE_AUTH_SALT', '%|yw[T;)^4^{{=.y*Q!-x)DAKtQtsH)RzUEEU,=;~W9DC 425&bRoSwL3^}[XAzC');
define('LOGGED_IN_SALT',   '|sDPx-~|}f<HXsqBR.CzMC9ZzkYJC)w[DB9rhql9)IZf_~8ys |A|+}?F>&^%+t|');
define('NONCE_SALT',       '>fgLl9z-at<q-4Py)?oYTpZ<G;;;AD[ `4|5)!]V=S{EFdi{I?+1RRp!=lCqn$44');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
